# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Chat room]
* Key functions (add/delete)
    1. [chat]
    2. [load message history]
    3. [private chat with online user]
* Other functions (add/delete)
    1. [create new room]
    2. [upload profile picture]
    3. [set up userid]
    4. [delete message history over 2 days automatically]
    5. [slider panel]
    6. [selector in chatroom]
    7. [press enter to send message]
    8. [click online useremail to display private message]
    9. [notify when send/receive a message]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
+ 首頁CSS使用template
+ 可使用一般信箱登入以及Google帳號登入
+ 註冊時在database中的userlist內push使用者的email以及默認頭相
+ 使用者登入後，會將userlist中的online值改成true；
  登出時則改成false，用此來判斷使用者是否在線上
+ 使用者登入後，nav Account內可選擇登出或是設定頭相、暱稱
+ 設定頭相時先將使用者所選圖片預覽，並將該圖傳到firebase storage
  將useremail變成key來儲存圖片，並將所有含有使用者頭像的歷史訊息更新
+ 設定暱稱時重新set firebase中userlist內的data，使其username更新
+ 可以在聊天室中新增其他公開聊天室，並用selector來紀錄此時在哪個房間
  以此來讀寫相對應的訊息
+ 聊天室右手邊有滑動式視窗來查看此時誰在線上，此滑動部分使用template；
  當點擊在線使用者的email後，會出現私訊該使用者的小視窗於左下角
  輸入訊息後鍵盤按Enter即可傳送，再點擊一次email即可關閉小視窗
+ 即使沒有開啟小視窗，仍可藉由notification來得知哪個使用者私訊給自己
+ 在load歷史訊息時，用spinner來完成CSS animation

## Security Report (Optional)
- 關閉其他網域的登入授權，只允許通過gitlab網域登入帳號
- 只有登入帳號才可寫入資料到網站中：
- 在大廳以及公開聊天室，需先經過firebase.auth()中的onAuthStateChanged來判斷是否是登入狀態，以此來決定是否有權輸入訊息到聊天室中。
- 私聊部分另外在firebase上創建一個list，該list內先根據user的email創出一串useremail，在每個useremail內有其他不同的useremail當child，結構大致上如右圖。
![Alt text](ex.png)
- 當有一方輸入訊息時，會在firebase list相對應的對方email下push訊息，同時也在對方那push自己輸入的訊息，載入歷史訊息時根據自己以及對方的email來抓取data，因此只抓取的到私訊別人、別人私訊自己的訊息