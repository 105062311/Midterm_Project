var a;
function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var logo = document.getElementById('logo');
        
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            logo.innerHTML = "<h2>" + user.email + "</h2>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function (snapshot) {
                    var userRef = firebase.database().ref('user_list').orderByChild("useremail");
                    userRef.once('value').then(function (snapshot) {
                        snapshot.forEach(function (childSnapshot) {
                            if (user.email == childSnapshot.child('useremail').val()) {
                                firebase.database().ref('user_list/' + childSnapshot.key).set({
                                    username: childSnapshot.child('username').val(),
                                    useremail: childSnapshot.child('useremail').val(),
                                    profile_picture: childSnapshot.child('profile_picture').val(),
                                    online: false
                                });
                            }
                        });
                    })
                    console.log('Logout sussess!');
                }).catch(function (error) {
                    console.log(error.message);
                });
            })
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    room_btn = document.getElementById('btnCreate');
    room_txt = document.getElementById('inputRoomname');
    var roomRef = firebase.database().ref('room_list');
    room_btn.addEventListener('click', function () {
        if (room_txt.value != "") {
            roomRef.push(room_txt.value);
            room_txt.value = "";
            window.location = "room.html";
        }
    });
}

window.onload = function () {
    init();
};
