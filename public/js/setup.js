var a;
var user_id = "";
function readImage(input) {
    if (input.files && input.files[0]) {
        var FR = new FileReader();
        FR.onload = function (e) {
            //e.target.result = base64 format picture
            $('#img').attr("src", e.target.result);
        };
        FR.readAsDataURL(input.files[0]);
    }
}

function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            var user_email = document.getElementById('user_email');
            user_email.innerHTML = user.email;
            var userRef = firebase.database().ref('user_list').orderByChild("useremail");
            userRef.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    if (user.email == childSnapshot.child('useremail').val()) {
                        console.log(childSnapshot.child('username').val());
                        user_id = childSnapshot.child('username').val();
                    }
                });
            })
            console.log(user_id);
            menu.innerHTML = "<span class='dropdown-item'>" + user_id + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById("logout-btn");
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function (snapshot) {
                    console.log('Logout sussess!');
                    var userRef = firebase.database().ref('user_list').orderByChild("useremail");
                    userRef.once('value').then(function (snapshot) {
                        snapshot.forEach(function (childSnapshot) {
                            if (user.email == childSnapshot.child('useremail').val()) {
                                firebase.database().ref('user_list/' + childSnapshot.key).set({
                                    username: childSnapshot.child('username').val(),
                                    useremail: childSnapshot.child('useremail').val(),
                                    profile_picture: childSnapshot.child('profile_picture').val(),
                                    online: false
                                });
                            }
                        });
                    })
                    window.location = "room.html";
                }).catch(function (error) {
                    console.log(error.message);
                });
            })

            var btnCreate = document.getElementById("create-btn");
            btnCreate.addEventListener('click', function () {
                var storageRef = firebase.storage().ref();

                var inputname = document.getElementById("inputusername");
                var file = document.querySelector('#inputimg').files[0];


                /*upload img to storage*/
                const metadata = {
                    contentType: file.type
                };
                var task = storageRef.child(user.email).put(file, metadata);

                /*get imgurl*/
                task.then(function (snapshot) {
                    var url = snapshot.downloadURL;
                    console.log('Uploaded a blob or file!' + url);

                    var userRef = firebase.database().ref('user_list').orderByChild("useremail");
                    userRef.once('value').then(function (snapshot) {
                        snapshot.forEach(function (childSnapshot) {
                            if (user.email === childSnapshot.child('useremail').val()) {
                                console.log(childSnapshot.key);
                                firebase.database().ref('user_list/' + childSnapshot.key).set({
                                    username: inputname.value,
                                    useremail: user.email,
                                    profile_picture: url,
                                    online:true
                                })
                            }
                        });
                    })
                    var roomRef = firebase.database().ref('room_list');
                    roomRef.once('value').then(function (snapshot) {
                        snapshot.forEach(function (childSnapshot) {
                            var comRef = firebase.database().ref('com_list/'+ childSnapshot.val());
                            comRef.once('value').then(function (snapshot2) {
                                snapshot2.forEach(function (childSnapshot2) {
                                    if (user.email === childSnapshot2.child('email').val()) {
                                        firebase.database().ref('com_list/' +childSnapshot.val()+'/'+ childSnapshot2.key).set({
                                            picture: url,
                                            data: childSnapshot2.child('data').val(),
                                            id: childSnapshot2.child('id').val(),
                                            email: childSnapshot2.child('email').val(),
                                            time: childSnapshot2.child('time').val(),
                                            n: childSnapshot2.child('n').val()
                                        }).then(function () {
                                            console.log("com updata!");
                                        }).catch((error) => {
                                            console.error(error);
                                        });
                                    }
                                });
                            })

                        })
                    })
                    alert("update success!");
                })
                

        })


} else {
    menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
}
    });



}

window.onload = function () {
    init();
};
