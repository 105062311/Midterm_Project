var exist = false;
function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(e =>
            window.location = "room.html")
            .catch(e => window.alert(e.message),
                email = "",
                password = ""
            );
        var userRef = firebase.database().ref('user_list').orderByChild("useremail");
        userRef.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                if (firebase.auth().currentUser.email == childSnapshot.child('useremail').val()) {
                    firebase.database().ref('user_list/' + childSnapshot.key).set({
                        username: childSnapshot.child('username').val(),
                        useremail: childSnapshot.child('useremail').val(),
                        profile_picture: childSnapshot.child('profile_picture').val(),
                        online: true
                    });
                }
            })

        })

    });

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"

        var provider = new firebase.auth.GoogleAuthProvider();

        btnGoogle.addEventListener('click', e => {
            console.log('signInWithPopup');
            firebase.auth().signInWithPopup(provider).then(function (result) {
                var token = result.credential.accessToken;
                var user = result.user;
                var userRef2 = firebase.database().ref('user_list').orderByChild("useremail");
                userRef2.once('value').then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        if (firebase.auth().currentUser.email == childSnapshot.child('useremail').val()) {
                            console.log("exist" + childSnapshot.child('useremail').val());
                            exist = true;

                            firebase.database().ref('user_list/' + childSnapshot.key).set({
                                username: childSnapshot.child('username').val(),
                                useremail: childSnapshot.child('useremail').val(),
                                profile_picture: childSnapshot.child('profile_picture').val(),
                                online: true
                            });

                            window.location = "room.html";
                        }
                    })
                    if (exist == false) {
                        var userRef = firebase.database().ref('user_list');
                        userRef.push({
                            username: firebase.auth().currentUser.email,
                            useremail: firebase.auth().currentUser.email,
                            profile_picture: "images/user.png",
                            online: true
                        }).then(function () {
                            console.log("push success");
                            window.location = "room.html";
                        }).catch((error) => {
                            console.error(error);
                        });
                    }
                })
            }).catch((error) => {
                console.error(error);
            });
        });
    });

    btnSignUp.addEventListener('click', function () {
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
        var userRef = firebase.database().ref('user_list');
        userRef.push({
            username: txtEmail.value,
            useremail: txtEmail.value,
            profile_picture: "images/user.png",
            online: false
        });
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function () {
            window.alert("Sign up success!");
            email = "";
            password = "";
        }).catch(e => console.error(error));
    });

}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};
