var a;
var nowroom = "";
var user_id = "";
var user_email = "";
var targetname_name = "";
function room(Obj) {
    $(".roomlist").removeClass("current_page_item");
    $(Obj).addClass("current_page_item");
}
function popup(Obj) {


    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            console.log("pop");
            var target = Obj.innerHTML;
            var div = document.getElementById("private_container");
            var targetname = document.getElementById("targetname");
            var targetRef = firebase.database().ref('target_list');


            targetname.innerHTML = "<p id='nametarget' style='margin-left:1em'>" + target + "</p>";
            targetname_name = document.getElementById("nametarget").innerHTML;
            /*-----------history private post--------------*/

            var str_before = "<div><div class='private_list'><p>";
            var str_after = "</strong></p></div></div>\n";

            // List for store posts html
            var target_post = [];
            // Counter for checking history post update complete
            var first_target_count = 0;
            // Counter for checking when to update new post
            var second_target_count = 0;
            var trans_me = "";
            for (var i = 0; i < user_email.length; i++) {
                if (user_email[i] == ".") trans_me += "-";
                else trans_me += user_email[i];
            }
            var trans_you = "";
            for (var i = 0; i < targetname_name.length; i++) {
                if (targetname_name[i] == ".") trans_you += "-";
                else trans_you += targetname_name[i];
            }
            targetRef.child(trans_me + "/" + trans_you).once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        console.log(childData.email);
                        target_post[target_post.length] = str_before + childData.email + "  " + childData.time + "<strong class='d-block text-gray-dark'>" + childData.data + str_after;
                        first_target_count += 1;
                        console.log("load");
                    });
                    document.getElementById('private').innerHTML = target_post.join('');

                    targetRef.child(trans_me + "/" + trans_you).on('child_added', function (data) {
                        second_target_count += 1;
                        if (second_target_count > first_target_count) {
                            var childData = data.val();
                            target_post[target_post.length] = str_before + childData.email + "  " + childData.time + "<strong class='d-block text-gray-dark'>" + childData.data + str_after;
                            document.getElementById('private').innerHTML = target_post.join('');
                            //  if (childData.email != user.email) notifyMe(childData.email);
                        }
                    });
                }).catch(e => console.log(e.message));

            /*-----------history private post--------------*/
            if (div.style.display == "none") {
                div.style.display = "block";
            } else {
                div.style.display = "none";
            }

        }
    })

}

function init() {

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            var userRef = firebase.database().ref('user_list').orderByChild("useremail");
            var b = 10;
            var str_before_onlineuser = "<ul class='onlinelist' style='bottom:";
            var str_between = "px'><a href='#' onclick='popup(this)' class='online'>"
            var str_after_onlineuser = "</a></ul>\n";
            var total_onlineuser = [];
            userRef.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    if (user.email == childSnapshot.child('useremail').val()) {
                        console.log(childSnapshot.child('username').val());
                        user_id = childSnapshot.child('username').val();
                        user_email = childSnapshot.child('useremail').val();
                        $("#userimg").attr("src", childSnapshot.child('profile_picture').val());
                    }
                    if (childSnapshot.child('online').val() == true) {
                        total_onlineuser[total_onlineuser.length] = str_before_onlineuser + b + str_between + childSnapshot.child('useremail').val() + str_after_onlineuser;
                        console.log("online");
                        b += 40;
                    }
                });
                document.getElementById('mwt_slider_content').innerHTML = total_onlineuser.join('');
            })
            userRef.on('value', function () {

            })
            console.log(user_id);
            menu.innerHTML = "<span class='dropdown-item'>" + user_id + "</span><span class='dropdown-item' id='logout-btn'>Logout</span><span class='dropdown-item' id='Setup-btn'>SetUp</span>";
            var btnLogout = document.getElementById("logout-btn");
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function (snapshot) {
                    console.log('Logout sussess!');
                    var userRef = firebase.database().ref('user_list').orderByChild("useremail");

                    userRef.once('value').then(function (snapshot) {
                        snapshot.forEach(function (childSnapshot) {
                            if (user.email == childSnapshot.child('useremail').val()) {
                                firebase.database().ref('user_list/' + childSnapshot.key).set({
                                    username: childSnapshot.child('username').val(),
                                    useremail: childSnapshot.child('useremail').val(),
                                    profile_picture: childSnapshot.child('profile_picture').val(),
                                    online: false
                                });
                            }
                        });
                    })


                }).catch(function (error) {
                    console.log(error.message);
                });
            })
            var btnSetup = document.getElementById("Setup-btn");
            btnSetup.addEventListener('click', e => {
                window.location = "setup.html";
            })

            post_btn = document.getElementById('post_btn');
            post_txt = document.getElementById('comment');

            $("#comment").on('keydown', function (e) {
                if (e.keyCode == 13) { //push enter
                    if (post_txt.value != "") { //迴避空字串
                        var postRef = firebase.database().ref('com_list');
                        var profileimg = document.getElementById("userimg").src;
                        var today = new Date();
                        var n = today.getDate();
                        var h = today.getHours();
                        var m = today.getMinutes();
                        var data = {
                            picture: profileimg,
                            data: post_txt.value,
                            id: user_id,
                            email: user_email,
                            time: "(" + h + ":" + m + ")",
                            n: n
                        };
                        console.log(nowroom[0].innerHTML);
                        postRef.child(nowroom[0].innerHTML).push(data);
                        post_txt.value = "";
                    }
                }
            });
            post_btn.addEventListener('click', function () {
                if (post_txt.value != "") {
                    var postRef = firebase.database().ref('com_list');
                    var profileimg = document.getElementById("userimg").src;
                    var today = new Date();
                    var n = today.getDate();
                    var h = today.getHours();
                    var m = today.getMinutes();
                    var data = {
                        picture: profileimg,
                        data: post_txt.value,
                        id: user_id,
                        email: user_email,
                        time: "(" + h + ":" + m + ")",
                        n: n
                    };
                    console.log(nowroom[0].innerHTML);
                    postRef.child(nowroom[0].innerHTML).push(data);
                    post_txt.value = "";
                }
            });

            private_txt = document.getElementById('comment_private');

            $("#comment_private").on('keydown', function (e) {
                if (e.keyCode == 13) { //push enter
                    if (private_txt.value != "") { //迴避空字串
                        var targetRef = firebase.database().ref('target_list');
                        var targetname = document.getElementById("nametarget").innerHTML;
                        var today = new Date();
                        var mon = today.getMonth() + 1;
                        var n = today.getDate();
                        var h = today.getHours();
                        var m = today.getMinutes();
                        var data = {
                            data: private_txt.value,
                            id: user_id,
                            email: user_email,
                            time: mon + "/" + n + "(" + h + ":" + m + ")"
                        };
                        var trans_me = "";
                        for (var i = 0; i < user_email.length; i++) {
                            if (user_email[i] == ".") trans_me += "-";
                            else trans_me += user_email[i];
                        }
                        var trans_you = "";
                        for (var i = 0; i < targetname.length; i++) {
                            if (targetname[i] == ".") trans_you += "-";
                            else trans_you += targetname[i];
                        }
                        targetRef.child(trans_me + "/" + trans_you).push(data);
                        targetRef.child(trans_you + "/" + trans_me).push(data);
                        private_txt.value = "";
                    }
                }
            });

            /*-----get the notification-------*/
            var targetRef = firebase.database().ref('target_list');
            var trans_me = "";
            for (var i = 0; i < user.email.length; i++) {
                if (user.email[i] == ".") trans_me += "-";
                else trans_me += user.email[i];
            }
            targetRef.child(trans_me).on('value', function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    targetRef.child(trans_me + '/' + childSnapshot.key).once('child_added').then(function (data) {
                        var childData = data.val();
                        if (childData.email !== user.email) notifyMe(childData.email);
                    })
                });
            })

            /*-----get the notification-------*/



        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });


    /*--------Roomlist--------*/

    var str_before_roomname = "<li class=''><a href='#' onclick='room(this)' class='roomlist'>";
    var str_after_roomname = "</a></li>\n";
    var roomRef = firebase.database().ref('room_list');
    var total_room = [];

    roomRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                if (childData == "Lobby") str_before_roomname = "<li class=''><a href='#' onclick='room(this)' class='roomlist current_page_item'>";
                else str_before_roomname = "<li class=''><a href='#' onclick='room(this)' class='roomlist'>";
                total_room[total_room.length] = str_before_roomname + childData + str_after_roomname;
            });
            total_room[total_room.length] = "<li class=''><a href='create.html'>Create chatroom</a></li>";
            document.getElementById('room_list').innerHTML = total_room.join('');
            nowroom = document.getElementsByClassName("current_page_item");
            console.log(nowroom[0].innerHTML);
            $("#room_list").on('click', function () {
                nowroom = document.getElementsByClassName("current_page_item");
                console.log(nowroom[0].innerHTML);

                /*-----------history post--------------*/

                var str_before_img = "<div class='row uniform'><div class='media text-muted pt-3'><div class='row'><img id='userimginpost' src='";
                var str_after_content = "</p></div></div></div>\n";
                var str_before_username = "<strong class='d-block text-gray-dark'>";

                var postsRef = firebase.database().ref('com_list');
                // List for store posts html
                var total_post = [];
                // Counter for checking history post update complete
                var first_count = 0;
                // Counter for checking when to update new post
                var second_count = 0;
                postsRef.child(nowroom[0].innerHTML).once('value')
                    .then(function (snapshot) {
                        snapshot.forEach(function (childSnapshot) {

                            var childData = childSnapshot.val();
                            console.log(childData.data);
                            total_post[total_post.length] = str_before_img + childData.picture + "'>" + str_before_username + childData.id + "</strong>" + childData.time + "<h4>" + childData.data + "</h4>" + str_after_content;
                            first_count += 1;

                        });
                        document.getElementById('post_list').innerHTML = total_post.join('');

                        postsRef.child(nowroom[0].innerHTML).on('child_added', function (data) {
                            second_count += 1;
                            if (second_count > first_count) {
                                var childData = data.val();
                                total_post[total_post.length] = str_before_img + childData.picture + "'>" + str_before_username + childData.id + "</strong>" + childData.time + "<h4>" + childData.data + "</h4>" + str_after_content;
                                document.getElementById('post_list').innerHTML = total_post.join('');
                                var div = document.getElementById('post_list');
                                //div.scrollTop = div.scrollHeight;
                                div.scrolltoBottom();

                            }
                        })

                    })

                /*-----------history post--------------*/
            })

            /*-----delete older post---------*/
            var comRef = firebase.database().ref('com_list');
            comRef.child(nowroom[0].innerHTML).once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var today_now = new Date();
                    var n_now = today_now.getDate();
                    if ((n_now - childSnapshot.child('n').val()) >= 2) {  //post time over 24h
                        console.log("remove start: data = " + childSnapshot.child('data').val());
                        comRef.child(nowroom[0].innerHTML + "/" + childSnapshot.key).remove().then(function () {
                            console.log("Remove succeeded.")
                        }).catch(function (error) {
                            console.log("Remove failed: " + error.message)
                        });
                    }
                });
            })
            /*-----delete older post---------*/
            /*-----------history post--------------*/

            var str_before_img = "<div class='row uniform'><div class='media text-muted pt-3'><div class='row'><img id='userimginpost' src='";
            var str_after_content = "</p></div></div></div>\n";
            var str_before_username = "<strong class='d-block text-gray-dark'>";

            var postsRef = firebase.database().ref('com_list');
            // List for store posts html
            var total_post = [];
            // Counter for checking history post update complete
            var first_count = 0;
            // Counter for checking when to update new post
            var second_count = 0;
            postsRef.child(nowroom[0].innerHTML).once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {

                        var childData = childSnapshot.val();
                        total_post[total_post.length] = str_before_img + childData.picture + "'>" + str_before_username + childData.id + "</strong>" + childData.time + "<h4>" + childData.data + "</h4>" + str_after_content;
                        first_count += 1;

                    });
                    document.getElementById('post_list').innerHTML = total_post.join('');

                    postsRef.child(nowroom[0].innerHTML).on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            total_post[total_post.length] = str_before_img + childData.picture + "'>" + str_before_username + childData.id + "</strong>" + childData.time + "<h4>" + childData.data + "</h4>" + str_after_content;
                            document.getElementById('post_list').innerHTML = total_post.join('');
                        }
                    });
                })
                .catch(e => console.log(e.message));

            /*-----------history post--------------*/

            /*-----------history private post--------------
            var targetRef = firebase.database().ref('target_list');
            var str_before = "<div><div class='private_list'><p>";
            var str_after = "</strong></p></div></div>\n";

            // List for store posts html
            var target_post = [];
            // Counter for checking history post update complete
            var first_target_count = 0;
            // Counter for checking when to update new post
            var second_target_count = 0;
            var trans_me = "";
            for (var i = 0; i < user_email.length; i++) {
                if (user_email[i] == ".") trans_me += "-";
                else trans_me += user_email[i];
            }
            var trans_you = "";
            for (var i = 0; i < targetname_name.length; i++) {
                if (targetname_name[i] == ".") trans_you += "-";
                else trans_you += targetname_name[i];
            }
            targetRef.child(trans_me + "/" + trans_you).once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        console.log(childData.email);
                        target_post[target_post.length] = str_before + childData.email + "  " + childData.time + "<strong class='d-block text-gray-dark'>" + childData.data + str_after;
                        first_target_count += 1;
                        console.log("load");
                    });
                    document.getElementById('private').innerHTML = target_post.join('');

                    targetRef.child(trans_me + "/" + trans_you).on('child_added', function (data) {
                        second_target_count += 1;
                        if (second_target_count > first_target_count) {
                            //     if(childData.email != user.email)notifyMe();
                            var childData = data.val();
                            target_post[target_post.length] = str_before + childData.email + "  " + childData.time + "<strong class='d-block text-gray-dark'>" + childData.data + str_after;
                            document.getElementById('private').innerHTML = target_post.join('');
                        }
                    });
                }).catch(e => console.log(e.message));


            -----------history private post--------------*/


        })





}

window.onload = function () {
    init();
};

/*-----notify------*/
document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.');
        return;
    }

    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function notifyMe(name) {
    if (Notification.permission !== "granted") {
        Notification.requestPermission();

    }
    else {
        var notification = new Notification('Chiu-Chat', {
            icon: 'images/icon.jpg',
            body: "You send/received a message to/from " + name,
        });

        notification.onclick = function () {
            window.open("https://105062311.gitlab.io/Midterm_Project/room.html");
        };

    }

}

    /*-----notify------*/